
package steps;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
//import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginSteps {

		WebDriver driver;
	
		@Before()
		public void setup() {
			//Set parameters for the webdriver and logging
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\brand\\Documents\\TimeSubmit\\TimeSubmit\\src\\test\\java\\resources\\chromedriver.exe");
			this.driver = new ChromeDriver (); //Declare chromedriver
			this.driver.manage().window().maximize();  //Maximizes the window
			this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS); //Sets timeout for pageload
			
		}
		
		@After()
		public void tearDown() {
			//Shut down the driver and all Chrome windows, and deletes cookies
			driver.manage().deleteAllCookies();
			driver.close();
			driver.quit();
		}

		

		@Given("^User navigates to Revature Timesheet$")
		public void user_navigates_to_Revature_Timesheet() throws Throwable {
		    driver.get("https://rev2.force.com/revature/s/login/");
		    Thread.sleep(4000);
		}

		@Given("^User enters a valid username$")
		public void user_enters_a_valid_username() throws Throwable {
		    driver.findElement(By.id("37:2;a")).sendKeys("[ENTER EMAIL HERE]");
		}

		@Given("^User enters a valid password$")
		public void user_enters_a_valid_password() throws Throwable {
			driver.findElement(By.id("47:2;a")).sendKeys("[ENTER PASSWORD HERE]");
		}

		@When("^User clicks on login button$")
		public void user_clicks_on_login_button() throws Throwable {
			driver.findElement(By.xpath("//button[contains(text(),'Log in')]")).click();
			Thread.sleep(5000);
		}

		@When("^User clicks on Open Current Timesheet button$")
		public void user_clicks_on_Open_Current_Timesheet_button() throws Throwable {
		    driver.findElement(By.xpath("//button[@title='Open Current Timesheet']")).click();
		    Thread.sleep(3000);
		}

		@When("^User enters their time for the week$")
		public void user_enters_their_time_for_the_week() throws Throwable {
			
			//Monday
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[4]/input")).clear();
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[4]/input")).sendKeys("1.2");
		    //Tuesday
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[5]/input")).clear();
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[5]/input")).sendKeys("3.4");
		    //Wednesday
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/input")).clear();
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[6]/input")).sendKeys("5.6");
		    //Thursday
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[7]/input")).clear();
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[7]/input")).sendKeys("7.8");
		    //Friday
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[8]/input")).clear();
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div[1]/div/table/tbody/tr[1]/td[8]/input")).sendKeys("9.0");
		   
		}

		@Then("^The user will submit their timesheet$")
		public void the_user_will_submit_their_timesheet() throws Throwable {

			driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
			//Get total
		    String total = driver.findElement(By.xpath("/html[1]/body[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[3]/td[2]/span[1]")).getText();
		    //See if total is correct.
		    Assert.assertEquals("27.00", total);
		    //Wait a generous amount of time so the user can see the output.
		    Thread.sleep(10000);
		}

		
	}

	


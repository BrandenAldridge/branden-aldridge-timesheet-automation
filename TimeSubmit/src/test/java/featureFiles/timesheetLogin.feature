Feature: Log into timesheet
	There should already be an existing timesheet
	The user should know their credentials

Scenario: Login to timesheet with correct credentials
Given User navigates to Revature Timesheet
When User enters a valid username
And User enters a valid password
And User clicks on login button
And User clicks on Open Current Timesheet button
And User enters their time for the week
Then The user will submit their timesheet

$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("timesheetLogin.feature");
formatter.feature({
  "line": 1,
  "name": "Log into timesheet",
  "description": "There should already be an existing timesheet\r\nThe user should know their credentials",
  "id": "log-into-timesheet",
  "keyword": "Feature"
});
formatter.before({
  "duration": 8056864000,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Login to timesheet with correct credentials",
  "description": "",
  "id": "log-into-timesheet;login-to-timesheet-with-correct-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "User navigates to Revature Timesheet",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User enters a valid username",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User enters a valid password",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "User clicks on login button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User should be taken to the timesheet",
  "keyword": "Then "
});
formatter.match({
  "location": "loginSteps.user_navigates_to_Revature_Timesheet()"
});
formatter.result({
  "duration": 8476961600,
  "status": "passed"
});
formatter.match({
  "location": "loginSteps.user_enters_a_valid_username()"
});
formatter.result({
  "duration": 1336001700,
  "status": "passed"
});
formatter.match({
  "location": "loginSteps.user_enters_a_valid_password()"
});
formatter.result({
  "duration": 283252800,
  "status": "passed"
});
formatter.match({
  "location": "loginSteps.user_clicks_on_login_button()"
});
formatter.result({
  "duration": 514842200,
  "status": "passed"
});
formatter.match({
  "location": "loginSteps.user_should_be_taken_to_the_timesheet()"
});
formatter.result({
  "duration": 2102200,
  "error_message": "cucumber.api.PendingException: TODO: implement me\r\n\tat steps.loginSteps.user_should_be_taken_to_the_timesheet(loginSteps.java:58)\r\n\tat ✽.Then User should be taken to the timesheet(timesheetLogin.feature:10)\r\n",
  "status": "pending"
});
formatter.uri("timesheetSubmit.feature");
formatter.feature({
  "line": 1,
  "name": "Enter hours into timesheet",
  "description": "Employee should have hours for the pay week to enter",
  "id": "enter-hours-into-timesheet",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4232554800,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Submit timesheet",
  "description": "",
  "id": "enter-hours-into-timesheet;submit-timesheet",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "User has logged into to their timesheet",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User clicks on Open Current Timesheet",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User enters time for each day",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "User clicks on Submit hours button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "User should see Timesheet Submitted Successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "submitSteps.user_has_logged_into_to_their_timesheet()"
});
formatter.result({
  "duration": 320900,
  "error_message": "cucumber.api.PendingException: TODO: implement me\r\n\tat steps.submitSteps.user_has_logged_into_to_their_timesheet(submitSteps.java:13)\r\n\tat ✽.Given User has logged into to their timesheet(timesheetSubmit.feature:5)\r\n",
  "status": "pending"
});
formatter.match({
  "location": "submitSteps.user_clicks_on_Open_Current_Timesheet()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "submitSteps.user_enters_time_for_each_day()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "submitSteps.user_clicks_on_Submit_hours_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "submitSteps.user_should_see_Timesheet_Submitted_Successfully()"
});
formatter.result({
  "status": "skipped"
});
});